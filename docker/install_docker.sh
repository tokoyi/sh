#!/bin/bash

echo "#########################################################################"
echo "# Install Docker CE (Install using the convenience script)              #"
echo "#########################################################################"

# Ref
#
# https://docs.docker.com/install/linux/docker-ce/ubuntu/
# https://github.com/docker/docker-install

# Other
#
# wget -qO- https://get.docker.com/ | sh

curl -fsSL https://get.docker.com -o get-docker.sh
sudo sh get-docker.sh
